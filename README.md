# Turicode Document Visualizer

![Head image](docs/head_img.png)

## Run the project

- Run `npm install` 

- If you encounter any errors, google for them and fix them.

- Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Project presentation
Project presentation can be found [here](docs/presentation.pdf).