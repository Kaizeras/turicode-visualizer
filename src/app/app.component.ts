import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'documentVisualizer';

  public nodes = [
    {
      id: '1',
      label: 'Doc 1'
    }, {
      id: '2',
      label: 'Doc 2'
    }, {
      id: '3',
      label: 'Doc 3'
    },
    {
      id: '4',
      label: 'Doc 4'
    }, {
      id: '5',
      label: 'Doc 5'
    },
    {
      id: '6',
      label: 'Doc 6'
    }, {
      id: '7',
      label: 'Doc 7'
    },
  ];

  public clusters = [
    {
      id: 'cluster0',
      label: 'Group 1',
      childNodeIds: ['1', '2', '3']
    },
    {
      id: 'cluster1',
      label: 'Group 2',
      childNodeIds: ['4', '5']
    },
    {
      id: 'cluster2',
      label: 'Group 3',
      childNodeIds: ['6', '7']
    }
  ];

  public clusteredNodes = this.nodes.map(node => {
    const cluster = this.clusters.find((c) => c.childNodeIds.includes(node.id));

    return {
      ...node,
      clusterName: cluster ? cluster.label : ''
    };
  });

}
